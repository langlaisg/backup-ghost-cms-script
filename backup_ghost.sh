#!/usr/bin/bash

# Backup Ghost CMS blog
# Syntax:
#    backup_ghost <Ghost Folder> <Backup folder>
#    backup_ghost /var/www/myblog/  /backup/

# The above command will create 2 filde into your /backup folder
# ls -l /backup
# -rw-rw-r-- 1 user user   144262 May  6 13:35 myblog-DB-2024-05-06_13:34:55.gzip
# -rw-rw-r-- 1 user user  5333853 May  6 13:34 myblog-content_2024-05-06_13:34:55.tar.gz


GHOST_DIR="${1}"
BACKUP_DIR="${2:-/backup}"  # default backup folder is /backup if you don't specify the second argument
BACKUP_DATE=$(date +"%Y-%m-%d_%T")


#Log script event
# Usage: 
#     log DEBUG|INFO|WARNING|ERROR|CRITICAL "message to log"
# Ex:
#     /bin/myCommand || log ERROR "Something wrong happen with the command /bin/myCommand."
#     log INFO  "This is an info message"
#
# Arg[1]:
#   INFO    : Information messages
#   WARNING : For warning or a non critical error
#   ERROR   : Error message not critical
#   CRITICAL: Error message (Any critical message cause the script to exit Code 1)
#
# Arg[2]:
#   Message to display between double quote : "This is my message"
log()
{

    local logType=${1}
    local msg=${2}
    local currentDate="$(date +"%Y-%m-%d %T")"
    [[ ${logType} == "DEBUG"    ]] && echo "${currentDate} - ${logType} - ${msg}"
    [[ ${logType} == "INFO"     ]] && echo "${currentDate} - ${logType} - ${msg}"
    [[ ${logType} == "WARNING"  ]] && echo "${currentDate} - ${logType} - ${msg}"
    [[ ${logType} == "ERROR"    ]] && echo "${currentDate} - ${logType} - ${msg}"
    [[ ${logType} == "CRITICAL" ]] && echo "${currentDate} - ${logType} - ${msg}" && exit 1

}


check_command_installation() {

    if ! command -v $1 &>/dev/null; then
        log ERROR "$1 is not installed"
        exit 0
    fi

}


# run checks
pre_backup_checks() {

    if [ ! -d "$GHOST_DIR" ]; then
        log CRITICAL "Ghost directory does not exist"
        exit 0
    fi

    log INFO "Running pre-backup checks"
    cd $GHOST_DIR

    cli=("expect" "gzip" "mysql" "mysqldump" "ghost" )
    for c in "${cli[@]}"; do
        check_command_installation "$c"
    done
    check_ghost_status

}


backup_content() {

    cd $GHOST_DIR || log ERROR "Can't cd into ${GHOST_DIR} !"
    BACKUP_NAME=$(basename ${GHOST_DIR})
    log INFO "Backup ${BACKUP_NAME}/content"
    tar --exclude='./content/apps' --exclude='./content/logs' -cvzf $BACKUP_DIR/${BACKUP_NAME}-content_${BACKUP_DATE}.tar.gz ./content || log ERROR "Backup of ${BACKUP_NAME} fail !"

}


check_mysql_connection() {

    log INFO "Checking MySQL connection..."
    if ! mysql -u"$mysql_user" -p"$mysql_password" -e ";" &>/dev/null; then
        log CRITICAL "Could not connect to MySQL"
        exit 0
    fi
    log INFO "MySQL connection OK"

}


backup_mysql() {

    log INFO "Backing up MySQL database"
    cd $GHOST_DIR

    mysql_user=$(ghost config get database.connection.user | tail -n1)
    mysql_password=$(ghost config get database.connection.password | tail -n1)
    mysql_database=$(ghost config get database.connection.database | tail -n1)

    check_mysql_connection

    log INFO "Dumping MySQL database..."
    log DEBUG "$mysql_user $mysql_database"
    mysqldump -u"$mysql_user" -p"$mysql_password" "$mysql_database" --no-tablespaces | gzip > "$BACKUP_DIR/$(basename $GHOST_DIR)-DB-${BACKUP_DATE}.gzip"

}


# MAIN
pre_backup_checks
backup_content
backup_mysql