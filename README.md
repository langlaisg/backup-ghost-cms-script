# Backup Ghost CMS Script



## Description

This script is to backup Ghost CMS via CLI **manually** or via **crontab**. 

## How to use it
### Install
```
cd ~
mkdir scripts && cd $_
git clone --depth=1 https://gitlab.com/langlaisg/backup-ghost-cms-script.git
cd backup-ghost-cms-script
chmod +x backup_ghost.sh
```

### Manual backup
```
./backup_ghost.sh /var/www/myBlog /backup
```

### Via crontab
The following will backup **myBlog1** the **fisrt day of every month at 0H00** in **/backup folder**

and **myBlog2** the **fisrt day of every month at 0H05**.
```
## Backup Ghost sites
0   0  1    *   *     /home/user/scripts/backup-ghost-cms-script/backupGhost.sh "/var/www/myBlog1/"
5   0  1    *   *     /home/user/scripts/backup-ghost-cms-script/backupGhost.sh "/var/www/myBlog2/"
```
